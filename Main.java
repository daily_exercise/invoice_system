import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Invoice>bill = new ArrayList<Invoice>();
        ArrayList<Product>bb = new ArrayList<Product>();

        bb.add(new Product(4,"huggies",87));
        bb.add(new Product(5,"huggies",87));
        bb.add(new Product(7,"huggies",87));
        bb.add(new Product(8,"huggies",87));

        bill.add(new Invoice(1,new Customer(1,"abc"), bb));


        // bill.add(new Invoice(9,new Customer(1,"abc"),new Product(4,"huggies",87)));
        // bill.add(new Invoice(8,new Customer(2,"bnj"),new Product(34,"ball",65)));
        // bill.add(new Invoice(6,new Customer(8,"xde"),new Product(3,"jug",29)));
        // bill.add(new Invoice(3,new Customer(8,"xde"),new Product(64,"mug",19)));

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Customer ID : ");

        int c_id = sc.nextInt();
        
        for (Invoice invoice : bill) {
            ArrayList<Product> pp = invoice.product;
            Customer cc = invoice.customer;
            if (c_id == cc.cId) {
                for (Product product : pp) {
                    System.out.println(product.pId+"\t"+product.pName+"\t"+product.price);
                }
                
            }
            
        }
    }
}
