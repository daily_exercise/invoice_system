public class Product {
    int pId;
    String pName;
    int price;

    public Product()
    {

    }

    public Product(int pId, String pName, int price)
    {
        this.pId=pId;
        this.pName=pName;
        this.price=price;
    }
}
